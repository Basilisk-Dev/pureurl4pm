var EXPORTED_SYMBOLS = ["Common","PureURLPrefListener"]
const {classes: Cc, interfaces: Ci, utils: Cu} = Components;
Cu.import("resource://gre/modules/Services.jsm");
Cu.import("resource://gre/modules/AddonManager.jsm");
Common={
		pureURLObserver : null,
		logoutput : false,
		linkcount : 0,
		expandcount: 0,
		prefBranch : Services.prefs.getBranch("extensions.pure-url@palemoon."),
		shorteners : null,
		currHost:"",
		ID : "pure-url@palemoon",
		uninstall : false,
		/**
		 * Toggle console logging.
		 */
		setLogging:function(flag){
			Common.logoutput = flag;
		},
		/**
		 * Log output to toolkit error console if enabled.
		 */ 
		printlog:function(item) {
			if(Common.logoutput==true) {
				Services.console.logStringMessage("[PureURL:]"+item);
			}
		},
		/**
		 * Install toolbar button.
		 */
		installButton:function(doc,toolbarId,id,afterId ){
			if (!doc.getElementById(id)) {
				var toolbar = doc.getElementById(toolbarId);
				var before = null;
				if (afterId) {
					let elem = doc.getElementById(afterId);
					if (elem && elem.parentNode == toolbar){
						before = elem.nextElementSibling;
					}
				}
				toolbar.insertItem(id,before);
				toolbar.setAttribute("currentset", toolbar.currentSet);
				doc.persist(toolbar.id, "currentset");
				if (toolbarId == "addon-bar"){
					toolbar.collapsed = false;
				}
			}
		},
		/**
		 * Remove persisted settings from XUL store.  
		 */
		removeWH:function(){
			let xulStore = Cc["@mozilla.org/xul/xulstore;1"].getService(Ci.nsIXULStore);
  			xulStore.removeValue("chrome://pureurl4pm/content/options.xul",
    							"pureurl-options-dialog", "width");
  			xulStore.removeValue("chrome://pureurl4pm/content/options.xul",
    							"pureurl-options-dialog","height");		
    	},
    /**
     * Check whether extension is being uninstalled and undo if not. 
     */
    uninstallListener :{
        onUninstalling:function(addon){
            if(addon.id==Common.ID){
                Common.uninstall=true;
            }
        },
        onOperationCancelled: function(addon) {
            if (addon.id == Common.ID) {
                Common.uninstall = (addon.pendingOperations & AddonManager.PENDING_UNINSTALL) != 0;
            }
        }   
    },
    /**
     * Clean up settings when browser is restarting after a confirmed uninstall.
     */
    uninstallObserver :{
        observe : function(aSubject,aTopic,aData){
            if(aTopic == "quit-application" && Common.uninstall == true) {
                Services.prefs.deleteBranch("extensions.pure-url@palemoon.");
            }
        }
    },
		GarbageFields:function(config){
			this.ok = false;
			this.db = {'*': []};
			this.no_locals = true;
			this.regexps = {'*': null};
			// Returns a proper regular expression for a host
			this.getRegExp = function(domain) {
				if (this.no_locals) {
					return this.regexps['*'];
				}
				if (this.regexps.hasOwnProperty(domain)) {
					return (typeof this.regexps[domain] === 'string') ? this.regexps[this.regexps[domain]] : this.regexps[domain];
				}
				var fields = this.db['*'];
				var subdomain = (domain !== undefined) ? domain : '';
				var store_as = null;
				var stored = false;
				while (subdomain) {
					if (this.db.hasOwnProperty(subdomain)) {
						if (!store_as) {
							store_as = subdomain;
							if (this.regexps.hasOwnProperty(store_as)) {
								stored = true;
								break;
							}
						}
						fields = fields.concat(this.db[subdomain]);
					}
					var dotpos = subdomain.indexOf('.');
					subdomain = (dotpos === -1) ? '' : subdomain.substring(dotpos+1);
				}
				if (store_as) {
					if (!stored) {
						this.regexps[store_as] = new RegExp('(^|&)(' + fields.join('|') + ')=[^&#]*', 'ig');
					}
					if (store_as !== domain) {
						this.regexps[domain] = store_as;
					}
				}
				else {
					if (!this.regexps['*'] && this.db['*'].length) {
						// Prepare the global fields regexp
						this.regexps['*'] = new RegExp('(^|&)(' + this.db['*'].join('|') + ')=[^&#]*', 'ig');
					}
					this.regexps[domain] = (fields.length === 0) ? null : '*';
				}

				return (typeof this.regexps[domain] === 'string') ? this.regexps[this.regexps[domain]] : this.regexps[domain];
			};

			//
			// Parse fields string
			this.add = function(newval) {
				if (typeof newval !== 'string' || !newval) return;

				// Filter illegal symbols and split fields
				newval = newval.replace(/[^-._@\d\w]+/ig, ' ').replace(/[\s]{2,}/g, ' ').replace(/(^[\s]+|[\s]+$)/g, '').split(' ');

				// Parse each field
				newval.forEach(function(field) {
					// Split and remove empty hosts
					var parts = field.split('@').filter(function(val, idx){ return !!val || idx === 0; });

					// Skip if field isn't present
					parts[0] = parts[0].replace(/[^_\d\w]+/ig, '');
					if (!parts[0]) return;

					// Add to global or local list
					this.ok = true;
					if (parts.length === 1) {
						if (this.db['*'].indexOf(parts[0]) === -1) {
							this.db['*'].push(parts[0]);
						}
					}
					else for (var i = 1; i < parts.length; i++) {
						parts[i] = parts[i].toLowerCase();
						if (this.db[parts[i]] === undefined) {
							this.no_locals = false;
							this.db[parts[i]] = [];
						}
						if (this.db[parts[i]].indexOf(parts[0]) === -1) {
							this.db[parts[i]].push(parts[0]);
						}
					}
				}, this);
				// Reset regexps cache
				this.regexps = {'*': null};
			}
			//
			// Read configuration
			if (typeof config === 'undefined' || !config) return;
			if (typeof config === 'string')	{
				this.add(config);
			}
			else if (typeof config === 'object'){
				// We have already parsed fields
				this.ok = true;
				this.db = config;
				this.no_locals = false;
				var entries = Object.getOwnPropertyNames(this.db);
				if (entries.length === 1){
					this.no_locals = (entries[0] === '*');
				}
				if (typeof this.db['*'] === 'undefined'){
					this.db['*'] = [];
				}
			}
		},
		gfields : null,
		checkedLinks:{},
		/**
		 * Recursively resolve unshortened links and add expanded original to tooltip.
		 */
		unshortenLink:function(el){
			let url = Services.io.newChannel2(el,null,null,null,
			                                  Services.scriptSecurityManager.getSystemPrincipal()
			                                  ,null,Ci.nsILoadInfo.SEC_NORMAL,Ci.nsIContentPolicy.TYPE_DOCUMENT);
			url.QueryInterface(Ci.nsIHttpChannel);
			url.requestMethod='HEAD';
			url.open();
			try{
				var response = url.getResponseHeader('Location');
				if(Math.round(url.responseStatus/100) == 3 && response){
					if(el != response) {
						Common.printlog("Resolved "+el+" to "+response);
					}
					return el==response ? response : Common.unshortenLink(response);
				}
			}catch(e){
				return el;
			}
		},
		/**
		 * Fixes links by traversing DOM.
		 */
		fixLinks : function(root){
			if(typeof (root)==='undefined') {
				return;
			}
			var els = root.nodeName.toLowerCase()==='a' ? [root] : root
					.querySelectorAll('a[href]');
			for(var i = 0; i<els.length; i++) {
				var el = els[i];
				var href = el.href;
				var host = el.host;
				var orig = el.href;
				if(!host)
					continue;
				var qpos = href.indexOf('?');
				if(qpos===-1||Common.checkedLinks.hasOwnProperty(href)){
					if(typeof el != 'undefined' && Common.prefBranch.getBoolPref("expandlinks") 
							&& Common.shorteners.has(host)){ 
						el.href = Common.unshortenLink(el);
						if(orig != el.href){
							Common.printlog("Original link: "+orig
							                        +"\nExpanded link: "+el.href);
							el.title=el.href;
							Common.expandcount++;
							href = el.href;
						}
					}
				}
				if(Common.expandcount > 0){
					qpos = href.indexOf('?'); //check expanded URL for params and only process if present.
				}
				if(qpos > -1){
					var args = href.substring(qpos+1, href.length);
					var regexp = Common.gfields.getRegExp(host);
					if(regexp) {
						var cleaned = args.replace(regexp, '').replace(/^[&]+/i, '');
						if(args!==cleaned) {
							href = href.substring(0, qpos);
							if(cleaned)
								href += '?'+cleaned;
							el.href = href;
							// show dotted line under cleaned URLs
							el.style.setProperty('border-bottom', '1px dotted #9f9f8e', 'important');
							// set link attribute that can be used for CSS userstyle
							el.setAttribute('pureurl-original', orig);
							Common.linkcount++;
							Common.printlog("Original link: "+orig+"\nCleaned link:"+href);
						}
					}
					Common.checkedLinks[el.href] = true;
				}
			}
			return root;
		}
}
function pureURLHttpObserver() {
}
pureURLHttpObserver.prototype = {
		observe : function(aSubject,aTopic,aData){
			if ("http-on-modify-request" == aTopic && 
					Common.prefBranch.getBoolPref("request_hook_enabled",true)) {
				aSubject.QueryInterface(Ci.nsIHttpChannel);
				url = aSubject.URI.spec;
				path = aSubject.URI.path;
				qpos = path.indexOf('?');
				regexp = Common.gfields.getRegExp(aSubject.URI.host);
				if (regexp !== null && qpos > -1) {
					var args = path.substring(qpos + 1, path.length);
					var cleaned = args.replace(regexp, '').replace(/^[&]+/i, '');
					if (args !== cleaned) {
						path = path.substring(0, qpos);
						if (cleaned) path += '?' + cleaned;
						try {
							aSubject.URI.path = path;
							Common.printlog("Original request: "+url+"\nCleaned request:"+aSubject.URI.spec);
						} catch (e) {
							Cu.reportError("[PureURL error:] URI= "+aSubject.URI.spec+"\nmodified path="+path+"\n"+e);
						}
					}
				}
			}
		}
}
Common.pureURLObserver = new pureURLHttpObserver();
function PureURLPrefListener(branch_name, callback) {
	// Keeping a reference to the observed preference branch or it will get
	// garbage collected.
	this._branch = Services.prefs.getBranch(branch_name);
	this._branch.QueryInterface(Ci.nsIPrefBranch2);
	this._callback = callback;
}

PureURLPrefListener.prototype.observe = function(subject, topic, data) {
	if (topic == 'nsPref:changed')
		this._callback(this._branch, data);
};

PureURLPrefListener.prototype.register = function() {
	this._branch.addObserver('', this, false);
	let that = this;
	this._branch.getChildList('', {}).
	forEach(function (pref_leaf_name){ 
		that._callback(that._branch, pref_leaf_name); 
	});
};

PureURLPrefListener.prototype.unregister = function() {
	if (this._branch)
		this._branch.removeObserver('', this);
};	