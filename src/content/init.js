if ("undefined" == typeof(PureURL)) {
	var PureURL = {};
}
Components.utils.import("chrome://pureurl4pm/content/common.jsm", PureURL);
const {classes: Cc, interfaces: Ci, utils: Cu} = Components;
Cu.import("resource://gre/modules/Services.jsm");
Cu.import("resource://gre/modules/XPCOMUtils.jsm");
Cu.import("resource://gre/modules/AddonManager.jsm");
const puPrefBranch = PureURL.Common.prefBranch;
PureURL.Launch={
		btnchkIntercept : null,
		btnchkExpandLinks : null,
		btnchkFixLinks : null,
		cmdExpandLinks : null,
		cmdFixLinks : null,
		cmdFixLinksType : null,
		cmdBtnTbToggle : null,
		cmdMnuToolsMenu : null,
		rbFixLinksDynamic : null,
		rbFixLinksStatic : null,
		mnuToolsMenu : null,
		btnPureURL : null,
		/**
		 * Toggles disabling radio buttons and expand links,<br>
		 * depending on whether fix links on pages is turned off.
		 */
		handleRadioButtons : function(linkfix){
			if(linkfix==true && PureURL.Launch.cmdFixLinksType!=null && PureURL.Launch.cmdExpandLinks!=null){
				PureURL.Launch.cmdFixLinksType.removeAttribute('disabled');
				PureURL.Launch.cmdExpandLinks.removeAttribute('disabled');
			} else {
				PureURL.Launch.cmdFixLinksType.setAttribute('disabled','true');
				PureURL.Launch.cmdExpandLinks.setAttribute('disabled','true');
			}
		},
		/**
		 * Display options dialog.
		 */
		showOptions : function() {
			if (null == this.optionsDialog || this.optionsDialog.closed) {
				let instantApply =
					Services.prefs.getBoolPref("browser.preferences.instantApply",true);
				let features =
					"chrome,titlebar,centerscreen" +
					(instantApply.value ? ",dialog=no" : ",modal");
				this.optionsDialog =window.openDialog("chrome://pureurl4pm/content/options.xul","Pure URL Options",
				                                      features);
				this.optionsDialog.focus();
			}
		},
		/**
		 * Toolbar handler for extension button addition/removal.
		 */
		toolbarHandler : {
			buttonAbsent : true,
			toolbarBeforeCustomization : function(event){
				if(PureURL.Launch.btnPureURL){
					buttonAbsent=false;
				}
			},
			toolbarAfterCustomization : function(event){
				PureURL.Launch.btnPureURL=document.getElementById('button-pureurl4pm');
				if(PureURL.Launch.btnPureURL==null){
					PureURL.Launch.buttonUnload();
				} else if(PureURL.Launch.btnPureURL && PureURL.Launch.toolbarHandler.buttonAbsent) {
					PureURL.Launch.buttonInit(puPrefBranch.getBoolPref("expandlinks",true),
					                          puPrefBranch.getBoolPref("request_hook_enabled",true),
					                          puPrefBranch.getCharPref("page_inject_type",""));	
				}
			}
		},
		/**
		 * Listens for preference changes and applies them to the UI.
		 */
		prefListener : new PureURL.PureURLPrefListener("extensions.pure-url@palemoon.",function(branch,name){
			switch(name){
			case("request_hook_enabled"):
				var intercept = puPrefBranch.getBoolPref("request_hook_enabled",true);
			PureURL.Common.pureURLObserver.intercept = intercept;
				if(PureURL.Launch.btnchkIntercept!=null){
					if(intercept){
						PureURL.Launch.btnchkIntercept.setAttribute('checked',true);
					} else {
						PureURL.Launch.btnchkIntercept.removeAttribute('checked');
					}
				}
			break;
			case("expandlinks"):
				var expand = puPrefBranch.getBoolPref("expandlinks",true);
				if(PureURL.Launch.btnchkExpandLinks!=null){
					if(expand){
						PureURL.Launch.btnchkExpandLinks.setAttribute('checked',true);
					} else {
						PureURL.Launch.btnchkExpandLinks.removeAttribute('checked');
					}
				}
				break;
			case("shorteners"):
				PureURL.Common.shorteners=new Set(puPrefBranch.getCharPref("shorteners","").split(",").sort());
				break;
			case("logging"):PureURL.Common.setLogging(puPrefBranch.getBoolPref("logging",true));
				break;
			case("toolsmenu"):if(PureURL.Launch.mnuToolsMenu!=null){ 
				PureURL.Launch.mnuToolsMenu.setAttribute('hidden',!puPrefBranch.getBoolPref("toolsmenu",true));
			}
				break;
			case("page_inject_type"):
				if(PureURL.Launch.btnPureURL!=null){
					var injectType = puPrefBranch.getCharPref("page_inject_type","");
					var appcontent = null;
					PureURL.Launch.handleRadioButtons(injectType.length > 0);
					if(injectType.length > 0) {
						appcontent=document.getElementById("appcontent");
						if(appcontent){
							appcontent.addEventListener("DOMContentLoaded", PureURL.Launch.pageModifier.onPageLoad, true);
						}
						if(PureURL.Launch.btnchkFixLinks!=null){
							PureURL.Launch.btnchkFixLinks.setAttribute('checked',true);
							if(injectType=="observe"){
								PureURL.Launch.rbFixLinksDynamic.setAttribute('checked',true);
							} else PureURL.Launch.rbFixLinksStatic.setAttribute('checked',true);
						}
					} else {
						appcontent=document.getElementById("appcontent");
						if(appcontent){
							appcontent.removeEventListener("DOMContentLoaded", PureURL.Launch.pageModifier.onPageLoad, true);
						}
						if(PureURL.Launch.btnchkFixLinks!=null){
							PureURL.Launch.btnchkFixLinks.removeAttribute('checked');
						}
					}
				}
			break;
			case("garbage_fields"):PureURL.Common.gfields=new PureURL.Common.GarbageFields(puPrefBranch.getCharPref("garbage_fields",""));
			break;
			default : break;
			}
				var inactive = puPrefBranch.getCharPref("page_inject_type","").length == 0 
				|| !puPrefBranch.getBoolPref("request_hook_enabled",true)
				|| !puPrefBranch.getBoolPref("expandlinks",true);
				PureURL.Launch.toggleIcons(!inactive);
		}),
		toggleIcons : function(active){
				if(PureURL.Launch.btnPureURL!=null){
					PureURL.Launch.btnPureURL.setAttribute("active",active? "true":"false");
				}
				if(PureURL.Launch.mnuToolsMenu != null && !PureURL.Launch.mnuToolsMenu.hidden){
					PureURL.Launch.mnuToolsMenu.setAttribute("active",active? "true":"false");
				}
		},
		/**
		 * Rich tooltip handler for displaying status on toolbar button.
		 */
		htmlTooltipHandler : {
			pageInject:"",
			plural:"",
			tooltiptext:"",
			statusInject:"",
			statusIntercept:"",
			strBundle : null,
			onLoad: function() {
				pageInject = puPrefBranch.getCharPref("page_inject_type","");
				statusExpand = puPrefBranch.getBoolPref("expandlinks",true) ? "On" : "Off";
				strBundle=document.getElementById('pureurl-strings');
				statusInject = pageInject == "" ? "Off":pageInject=='observe'?"Dynamic":"Static";
				statusIntercept=puPrefBranch.getBoolPref("request_hook_enabled",true)? "On":"Off";
				tooltiptext=strBundle.getFormattedString(
				                                         'showStatus',[statusExpand,
				                                        	 statusInject,
				                                        	 statusIntercept,
				                                        	 PureURL.Common.linkcount,
				                                        	 PureURL.Common.expandcount,
				                                        	 PureURL.Common.currHost]);
				if(PureURL.Launch.btnPureURL!=null){
					PureURL.Launch.btnPureURL.setAttribute("tooltipHTML", tooltiptext);
				}
			},
			onMouseTooltip: function(event) {
				pageInject = puPrefBranch.getCharPref("page_inject_type","");
				statusExpand = puPrefBranch.getBoolPref("expandlinks",true) ? "On" : "Off";
				statusInject = pageInject == "" ? "Off":pageInject=='observe'?"Dynamic":"Static";
				statusIntercept=puPrefBranch.getBoolPref("request_hook_enabled",true)? "On":"Off";
				tooltiptext=strBundle.getFormattedString(
				                                         'showStatus',[statusExpand,
				                                        	 statusInject,
				                                        	 statusIntercept,
				                                        	 PureURL.Common.linkcount,
				                                        	 PureURL.Common.expandcount,
				                                        	 PureURL.Common.currHost]);
				// get the HTML div element that is inside the custom XUL
				// tooltip
				var div = document.getElementById("myHTMLTipDiv");
				// clear the HTML div element of any prior shown custom HTML
				while(div.firstChild) 
					div.removeChild(div.firstChild);
				// safely convert HTML string to a simple DOM object, stripping
				// it of JavaScript and more complex tags
				var injectHTML = Cc["@mozilla.org/feed-unescapehtml;1"]
					.getService(Ci.nsIScriptableUnescapeHTML)
					.parseFragment(tooltiptext, false, null, div); 

				// attach the DOM object to the HTML div element
				div.appendChild(injectHTML); 
			}
		},
		/**
		 * Handler for modifying links on pages.
		 */
		pageModifier : {
				init : function(){
					var appcontent = document.getElementById("appcontent");
					if(appcontent){
						appcontent.addEventListener("DOMContentLoaded",
						                            PureURL.Launch.pageModifier.onPageLoad, true);
					} 
				},
				/**
				 * Fixes links on page load, adds mutation observer if fix links
				 * preference = 'Dynamic'.
				 */
				onPageLoad : function(event){ 
					var doc = Cu.getWeakReference(event.originalTarget).get();
					var win = doc.defaultView;
					var pageInject = puPrefBranch.getCharPref("page_inject_type","");
					if (doc.documentURI.startsWith("http") && pageInject!= "" && doc instanceof HTMLDocument && doc.nodeName == "#document" && win ==win.top && !win.frameElement) {
						PureURL.Common.currHost = Services.io.newURI(doc.documentURI,null,null).host;
						PureURL.Common.linkcount = 0;
						PureURL.Common.expandcount = 0;
						PureURL.Common.fixLinks(doc);
						if(pageInject=='observe'){
								var observer = new MutationObserver(function(mutations){
								var checked_els = [];
								mutations.forEach(function(mutation){
									if(mutation.type==='attributes') {
										if(mutation.target.nodeName.toLowerCase()==='a'
											&& mutation.attributeName==='href') {
												mutation.target = PureURL.Common.fixLinks(mutation.target);
										}
										return;
									}
									for(var i = 0; i < mutation.addedNodes.length; i++) {
										var el = mutation.addedNodes[i];
										// Fast checks
										if(!el||!(el instanceof Element)||el.parentNode===null
												||checked_els.indexOf(el)!==-1) {
											continue;
										}
										checked_els.push(el);
										// Slow checks
										if(!doc.body.contains(el)||!el.hasChildNodes()
												&&el.nodeName.toLowerCase()!=='a') {
											continue;
										}
										el = PureURL.Common.fixLinks(el);
									}
								});
							});
							observer.observe(doc.body, {
								childList : true,
								attributes : true,
								subtree : true
							});
						}
					}
				}
			},
		/**
		 * Initializes extension, called when window opened.
		 */
		init : function(){
			firstRun = puPrefBranch.getBoolPref("firstrun",true);
			if(firstRun){
				puPrefBranch.setBoolPref("firstrun",false);
				puPrefBranch.setBoolPref("logging",false);
				PureURL.Common.installButton(document,"nav-bar","button-pureurl4pm");
				PureURL.Common.installButton(document,"addon-bar","button-pureurl4pm");
			}
			PureURL.Common.removeWH();
			PureURL.Launch.prefListener.register();
			cmdBtnTbToggle=document.getElementById('CmdPureURL4PMToggle');
			PureURL.Launch.cmdMnuToolsMenu=document.getElementById('CmdPureURL4PMOptions');
			PureURL.Launch.cmdMnuToolsMenu.addEventListener('command',PureURL.Launch.showOptions);
			PureURL.Launch.btnPureURL=document.getElementById('button-pureurl4pm');
			PureURL.Launch.mnuToolsMenu=document.getElementById('menu-pureurl4pm-options');
			PureURL.Common.gfields=new PureURL.Common.GarbageFields(puPrefBranch.getCharPref("garbage_fields",""));
			PureURL.Common.shorteners=new Set(puPrefBranch.getCharPref("shorteners","").split(",").sort());
			PureURL.Common.setLogging(puPrefBranch.getBoolPref("logging",true));
			var expand = puPrefBranch.getBoolPref("expandlinks",true);
			var intercept = puPrefBranch.getBoolPref("request_hook_enabled",true);
			var injectType = puPrefBranch.getCharPref("page_inject_type","");
			Services.obs.addObserver(PureURL.Common.pureURLObserver,"http-on-modify-request",false);
			if(PureURL.Launch.btnPureURL!=null){
				PureURL.Launch.buttonInit(expand,intercept,injectType);
			}
			PureURL.Launch.mnuToolsMenu.setAttribute('hidden',!puPrefBranch.getBoolPref("toolsmenu",true));
			PureURL.Launch.toggleIcons(expand && intercept && injectType.length > 0);
			gBrowser.addProgressListener(PureURL.Launch.tabSwitchListener);
			PureURL.Launch.checkURI(gBrowser.currentURI);
      AddonManager.addAddonListener(PureURL.Common.uninstallListener);
      Services.obs.addObserver(PureURL.Common.uninstallObserver, "quit-application", false);
		},
		/**
		 * Event handler for intercept link toggle toolbar button checkbox.
		 */
		toggleInterceptChk : function(){
			puPrefBranch.setBoolPref("request_hook_enabled",PureURL.Launch.btnchkIntercept.hasAttribute("checked"));		
		},
		/**
		 * Event handler for expand links toggle toolbar button checkbox.
		 */
		toggleExpandLinks : function(){
			puPrefBranch.setBoolPref("expandlinks",PureURL.Launch.btnchkExpandLinks.hasAttribute("checked"));
		},
		/**
		 * Event handler for radio buttons to select fix links type.
		 */
		selectFixLinksType : function(){
			var fixtype = PureURL.Launch.rbFixLinksDynamic.hasAttribute('checked')?"observe":"onload";
			puPrefBranch.setCharPref("page_inject_type",fixtype);
		},
		/**
		 * Event handler for toolbar button checkbox for fix links toggle.
		 */
		toggleFixLinks : function(event){
			var checked = PureURL.Launch.btnchkFixLinks.hasAttribute("checked");
			var fixtype="";
			if(checked){
				fixtype = PureURL.Launch.rbFixLinksDynamic.hasAttribute('checked')?"observe":"onload";
				PureURL.Launch.btnchkExpandLinks.setAttribute("checked","true");
			} else {
				PureURL.Launch.btnchkExpandLinks.removeAttribute("checked");
			}
			puPrefBranch.setCharPref("page_inject_type",fixtype);
			puPrefBranch.setBoolPref("expandlinks",checked);
			PureURL.Launch.handleRadioButtons(checked);
		},
		/**
		 * Event handler for overall toggle on pressing toolbar button.
		 */
		toggleEnable : function(){
			var expand = puPrefBranch.getBoolPref("expandlinks",true);
			var intercept = puPrefBranch.getBoolPref("request_hook_enabled",true);
			var injectType = puPrefBranch.getCharPref("page_inject_type","");
			var active = expand && intercept && injectType.length > 0;
			if(active){
				puPrefBranch.setCharPref("page_inject_type","");
				puPrefBranch.setBoolPref("request_hook_enabled",false);
				puPrefBranch.setBoolPref("expandlinks",false);
			} else {
				var type = PureURL.Launch.rbFixLinksDynamic.hasAttribute('checked')?"observe":"onload";
				puPrefBranch.setCharPref("page_inject_type",type);
				puPrefBranch.setBoolPref("request_hook_enabled",true);
				puPrefBranch.setBoolPref("expandlinks",true);
			}
		},
		/**
		 * Disable toolbar button/tooltip when on a non webpage.
		 */
		checkURI : function(uri){
			if(PureURL.Launch.btnPureURL!=null){
				if(uri.scheme.startsWith('http')){
					PureURL.Launch.btnPureURL.removeAttribute("disabled");
					PureURL.Launch.btnPureURL.addEventListener('mouseover',PureURL.Launch.htmlTooltipHandler.onMouseTooltip);
				} else {
					PureURL.Launch.btnPureURL.removeEventListener('mouseover',PureURL.Launch.htmlTooltipHandler.onMouseTooltip);
					PureURL.Launch.btnPureURL.removeAttribute("tooltiptext");
					PureURL.Launch.btnPureURL.setAttribute("disabled","true");
				}
				
			}
			if(PureURL.Launch.mnuToolsMenu!=null && !PureURL.Launch.mnuToolsMenu.hidden){
				if(uri.scheme.startsWith('http')){
					PureURL.Launch.mnuToolsMenu.removeAttribute("disabled");
				} else {
					PureURL.Launch.mnuToolsMenu.setAttribute("disabled","true");
				}
			}
		},
		tabSwitchListener : {
			QueryInterface: XPCOMUtils.generateQI(["nsIWebProgressListener","nsISupportsWeakReference"]),
			onStateChange: function(aWebProgress, aRequest, aFlag, aStatus) {
			},
			onLocationChange: function(aProgress, aRequest, aURI) {
				PureURL.Launch.checkURI(aURI);
			}
		},
		/**
		 * Sets up toolbar button when it is added.
		 */
		buttonInit : function(expand,intercept,injectType){
			PureURL.Launch.btnPureURL.addEventListener('mouseover',PureURL.Launch.htmlTooltipHandler.onMouseTooltip);
			PureURL.Launch.btnPureURL.setAttribute('oncommand','CmdPureURL4PMToggle');
			PureURL.Launch.btnchkIntercept = document.getElementById('chk-intercept-requests');
			PureURL.Launch.btnchkFixLinks=document.getElementById('chk-pureurl-fix-links');
			PureURL.Launch.rbFixLinksDynamic=document.getElementById('rb_page_inject_dynamic');
			PureURL.Launch.rbFixLinksStatic=document.getElementById('rb_page_inject_static');
			PureURL.Launch.btnchkExpandLinks=document.getElementById("chk-pureurl-expandlinks");
			PureURL.Launch.cmdFixLinks=document.getElementById('CmdPureURL4PMFixLinks');
			PureURL.Launch.cmdFixLinksType=document.getElementById('CmdPureURL4PMFixLinksType');
			PureURL.Launch.cmdExpandLinks=document.getElementById('CmdPureURL4PMExpandLinks');
			PureURL.Launch.cmdFixLinks.addEventListener('command',PureURL.Launch.toggleFixLinks);
			PureURL.Launch.cmdFixLinksType.addEventListener('command',PureURL.Launch.selectFixLinksType);
			PureURL.Launch.cmdExpandLinks.addEventListener('command',PureURL.Launch.toggleExpandLinks);
			document.getElementById('CmdPureURL4PMIntercept').addEventListener('command',PureURL.Launch.toggleInterceptChk);
			document.getElementById('CmdPureURL4PMToggle').addEventListener('command',PureURL.Launch.toggleEnable);
			if(intercept==true){
				PureURL.Launch.btnchkIntercept.setAttribute('checked','true');
			}
			if(expand==true){
				PureURL.Launch.btnchkExpandLinks.setAttribute('checked','true');
			}
			PureURL.Launch.handleRadioButtons(injectType.length > 0);
			if(injectType.length > 0) {
				PureURL.Launch.btnchkFixLinks.setAttribute('checked','true');
				switch(injectType){
				case "onload": PureURL.Launch.rbFixLinksStatic.setAttribute('checked',true);
				break;
				case "observe": PureURL.Launch.rbFixLinksDynamic.setAttribute('checked',true);
				break;
				}
			} else {
				PureURL.Launch.btnchkFixLinks.removeAttribute('checked');
				PureURL.Launch.rbFixLinksDynamic.setAttribute('checked',true);
			}
			var inactive = injectType.length == 0 || !intercept || !expand;
			PureURL.Launch.toggleIcons(!inactive);
      PureURL.Launch.checkURI(gBrowser.currentURI);
		},
		/**
		 * Unregisters toolbar button listeners when it is removed.
		 */
		buttonUnload : function(){
			if(PureURL.Launch.cmdFixLinks!=null){
				PureURL.Launch.cmdFixLinks.removeEventListener('command',PureURL.Launch.toggleFixLinks);
			}
			if(PureURL.Launch.cmdFixLinksType!=null){
				PureURL.Launch.cmdFixLinksType.removeEventListener('command',PureURL.Launch.selectFixLinksType);
			}
			document.getElementById('CmdPureURL4PMIntercept').removeEventListener('command',PureURL.Launch.toggleInterceptChk);
			document.getElementById('CmdPureURL4PMToggle').removeEventListener('command',PureURL.Launch.toggleEnable);
		},
		/**
		 * Cleanup listeners on closing window.
		 */
		cleanup : function(){
      Services.obs.removeObserver(PureURL.Common.uninstallObserver, "quit-application");
      AddonManager.removeAddonListener(PureURL.Common.uninstallListener);
			gBrowser.removeProgressListener(PureURL.Launch.tabSwitchListener);
			window.removeEventListener("load",PureURL.Launch.init);
			window.removeEventListener("load",PureURL.Launch.pageModifier.init);
			window.removeEventListener("load",PureURL.Launch.htmlTooltipHandler.onLoad);
			window.removeEventListener("aftercustomization",PureURL.Launch.toolbarHandler.toolbarAfterCustomization);
			Services.obs.removeObserver(PureURL.Common.pureURLObserver,"http-on-modify-request");
			window.removeEventListener("beforecustomization",PureURL.Launch.toolbarHandler.toolbarBeforeCustomization);
			document.getElementById('CmdPureURL4PMIntercept').removeEventListener('command',PureURL.Launch.toggleInterceptChk);
			PureURL.Launch.cmdExpandLinks.removeEventListener('command',PureURL.Launch.toggleExpandLinks);
			PureURL.Launch.cmdMnuToolsMenu.removeEventListener('command',PureURL.Launch.showOptions);
			PureURL.Launch.prefListener.unregister();
			if(PureURL.Launch.btnPureURL){
				PureURL.Launch.buttonUnload();
			}
		}
}
window.addEventListener("load", PureURL.Launch.init,false);
window.addEventListener('load', PureURL.Launch.htmlTooltipHandler.onLoad, false);
window.addEventListener("load", function load(){
	window.removeEventListener("load", load, false);
	PureURL.Launch.pageModifier.init();
},false);
window.addEventListener("unload", PureURL.Launch.cleanup,false);
window.addEventListener("aftercustomization",PureURL.Launch.toolbarHandler.toolbarAfterCustomization,false);
window.addEventListener("beforecustomization",PureURL.Launch.toolbarHandler.toolbarBeforeCustomization,false);